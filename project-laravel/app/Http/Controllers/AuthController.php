<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function welcome(Request $request){
        $ndepan = $request['first'];
        $nbelakang = $request['last'];
        return view('welcome1',compact('ndepan','nbelakang'));
    }
}
