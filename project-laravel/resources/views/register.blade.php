<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Buat Account Baru!</h2>
    <h4>Sign Up Form</h4>
    <form action="/welcome1" method="post">
        @csrf
        <p>First name:</p>
        <input type="text" name="first">
        <p>Last name:</p>
        <input type="text" name="last">
        <p>Gender:</p>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br>
        <p>Nationality:</p>
        <select>
            <option value="0">Indonesian</option>
            <option value="1">Singapore</option>
            <option value="2">Malaysian</option>
            <option value="3">Arabian</option>
        </select>
        <p>Language Spoken:</p>
        <input type="checkbox" name="bahasa">Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa">English <br>
        <input type="checkbox" name="bahasa">Other
        <p>Bio:</p>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>